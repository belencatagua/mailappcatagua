import 'package:email/model/Email.dart';
import 'package:email/screens/DetailScreen.dart';
import 'package:flutter/material.dart';

class EmailWidget extends StatelessWidget {
  final Email email;

  EmailWidget({required this.email});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          ListTile(
            trailing: Text(email.dateTime),
            title: Text(email.from),
            subtitle: Text(email.subject),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DetailScreen(emails: email)));
            },
          )
        ],
      ),
    );
  }
}
